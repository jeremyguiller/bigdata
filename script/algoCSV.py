import csv

# Step 1: Create a list of dictionaries
total = []
with open('../Data/styles.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        total.append(row)

# Step 2: Create a dictionary of parameter lists
params = {}
for row in total:
    for param in row:
        if param not in ['id', 'season', 'subCategory', 'productDisplayName', 'usage']:
            if param not in params:
                params[param] = set()
            params[param].add(row[param])

# Step 3: Define a function to filter the dataset based on user preferences
def filter_data(data, prefs):
    filtered_data = []
    for item in data:
        if all(item[param] == prefs[param] for param in prefs):
            filtered_data.append(item)
    return filtered_data

# Step 4: Define a function to ask user preferences
def ask_prefs(params):
    prefs = {}
    available_params = params.copy()
    for param in params:
        if param not in ['id', 'season', 'subCategory', 'productDisplayName', 'usage']:
            valid_choices = sorted(list(available_params[param]))
            choice = None
            while choice not in valid_choices:
                prompt = f"Select a {param} from {valid_choices}: "
                choice = input(prompt)
                if choice in available_params[param]:
                    # Remove the selected choice from the available options for subsequent parameters
                    for param2 in available_params:
                        if param2 != param:
                            available_params[param2] = available_params[param2].intersection(set([item[param2] for item in filter_data(total, {**prefs, param: choice})]))
                    # Record the user's choice
                    prefs[param] = choice
                else:
                    print("Invalid choice. Please try again.")
    return prefs

# Step 5: Define a function to recommend items based on user preferences
def recommend(data, prefs):
    scores = {}
    for item in data:
        score = 0
        for param in prefs:
            if item[param] == prefs[param]:
                score += 1
        scores[item['productDisplayName']] = score
    sorted_scores = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    recommended_names = [item[0] for item in sorted_scores[:5]]
    return recommended_names

# Example usage
# print("Welcome to the clothing recommendation system!")
# while True:
#     prefs = ask_prefs(params)
#     filtered_data = filter_data(total, prefs)
#     if len(filtered_data) == 0:
#         print("No items found matching your preferences. Please try again.")
#     else:
#         recommended_names = recommend(filtered_data, prefs)
#         print(f"Recommended items: {recommended_names}")
#     choice = input("Would you like to make another recommendation? (y/n): ")
#     if choice != 'y':
#         break
