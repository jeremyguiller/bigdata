# Clothing Recommender
Ce projet est une application de recommandation de vêtements basée sur les préférences de l'utilisateur. L'application permet aux utilisateurs de filtrer un ensemble de données de vêtements en fonction de leurs préférences et de recevoir des recommandations de vêtements pertinents. Le projet comprend deux parties principales : l'algorithme de recommandation de vêtements (algoCSV) et l'interface graphique utilisateur (ClothingRecommenderGUI).

## AlgoCSV
L'algorithme algoCSV est un programme Python qui lit un fichier CSV contenant des informations sur les vêtements, construit une liste de dictionnaires à partir de ces informations, filtre la liste en fonction des préférences de l'utilisateur, puis recommande les vêtements les plus pertinents. Voici les étapes clés de l'algorithme :

- Créer une liste de dictionnaires à partir du fichier CSV
- Créer un dictionnaire de listes de paramètres
- Définir une fonction pour filtrer les données en fonction des préférences de l'utilisateur
- Définir une fonction pour demander les préférences de l'utilisateur
- Définir une fonction pour recommander les vêtements en fonction des préférences de l'utilisateur
## ClothingRecommenderGUI

L'interface graphique utilisateur ClothingRecommenderGUI est un programme Python qui utilise l'algorithme algoCSV pour recommander des vêtements à l'utilisateur. L'interface graphique permet aux utilisateurs de sélectionner leurs préférences de vêtements à l'aide de menus déroulants et de boutons, puis affiche les recommandations de vêtements correspondantes. Voici les principales fonctionnalités de l'interface graphique :

- Menu déroulant pour la saison
- Menu déroulant pour la sous-catégorie
- Menu déroulant pour le type d'utilisation
- Bouton pour soumettre les préférences de l'utilisateur
- Affichage des recommandations de vêtements 


## Comment utiliser l'application 

Pour utiliser l'application, suivez les étapes suivantes :

Assurez-vous que vous avez Python 3 et les modules tkinter et csv installés sur votre ordinateur.
Téléchargez le code source de l'application depuis le dépôt GitHub.
Ouvrez une ligne de commande dans le dossier contenant les fichiers source.
Exécutez la commande "python ClothingRecommenderGUI.py" pour lancer l'interface graphique.
Sélectionnez vos préférences de vêtements à l'aide des menus déroulants.
Cliquez sur le bouton "Recommander" pour afficher les recommandations de vêtements correspondantes.
Auteurs
Ce projet a été créé par [votre nom] et [le nom de votre partenaire], étudiants en informatique à [votre université]. Si vous avez des questions ou des commentaires sur le projet, n'hésitez pas à nous contacter à [votre adresse e-mail].