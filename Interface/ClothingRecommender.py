import tkinter as tk
from script.algoCSV import *


class ClothingRecommenderGUI:
    def __init__(self):
        self.total = []
        with open('../Data/styles.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.total.append(row)

        self.params = {}
        for row in self.total:
            for param in row:
                if param not in ['id', 'season', 'subCategory', 'usage', 'productDisplayName']:
                    if param not in self.params:
                        self.params[param] = set()
                    self.params[param].add(row[param])

        self.root = tk.Tk()
        self.root.title("Clothing Recommender")

        self.choice_vars = {}
        self.choices = {}
        self.category_order = ["gender", "masterCategory", "articleType", "baseColour", "year"]
        for i, category in enumerate(self.category_order):
            valid_choices = sorted(list(self.filter_choices(category)))
            self.choice_vars[category] = tk.StringVar(value=valid_choices[0])
            self.choices[category] = tk.OptionMenu(self.root, self.choice_vars[category], *valid_choices,
                                                   command=lambda value, category=category: self.update_choices(
                                                       category))
            self.choices[category].grid(row=i, column=0, padx=10, pady=5)
            self.choices[category].config(width=20)

        self.result_label = tk.Label(self.root, text="")
        self.result_label.grid(row=len(self.category_order), column=0, pady=10)

        self.recommend_button = tk.Button(self.root, text="Recommend", command=self.recommend)
        self.recommend_button.grid(row=len(self.category_order)+1, column=0, pady=10)

        self.root.mainloop()

    def update_choices(self, category):
        index = self.category_order.index(category)
        for i in range(index + 1, len(self.category_order)):
            curr_category = self.category_order[i]
            valid_choices = sorted(list(self.filter_choices(curr_category)))
            self.choice_vars[curr_category].set(valid_choices[0])
            menu = self.choices[curr_category]['menu']
            menu.delete(0, 'end')
            for choice in valid_choices:
                menu.add_command(label=choice, command=lambda choice=choice, category=curr_category:
                self.choice_vars[category].set(choice))
        prefs = {}
        for param in self.category_order:
            prefs[param] = self.choice_vars[param].get()
        self.filtered_data = filter_data(self.total, prefs)

    def filter_choices(self, category):
        choices = self.params[category]
        if category == "gender":
            return choices
        prev_category = self.category_order[self.category_order.index(category) - 1]
        prev_choice = self.choice_vars[prev_category].get()
        filtered_data = filter_data(self.total, {prev_category: prev_choice})
        filtered_choices = set([row[category] for row in filtered_data])
        available_choices = choices.intersection(filtered_choices)
        return available_choices

    def recommend(self):
        prefs = {}
        for param in self.category_order:
            prefs[param] = self.choice_vars[param].get()

        filtered_data = self.filtered_data
        if len(filtered_data) == 0:
            # If no items are found matching the user's preferences, update the choices for the top category
            top_category = self.category_order[0]
            valid_choices = sorted(list(self.filter_choices(top_category)))
            self.choice_vars[top_category].set(valid_choices[0])
            menu = self.choices[top_category]['menu']
            menu.delete(0, 'end')
            for choice in valid_choices:
                menu.add_command(label=choice, command=lambda choice=choice, category=top_category:
                self.choice_vars[category].set(choice))
            self.filtered_data = filter_data(self.total, {top_category: self.choice_vars[top_category].get()})
            self.result_label.config(text="No items found matching your preferences. Please try again.")
        else:
            recommended_names = recommend(filtered_data, prefs)
            recommended_data = filter_data(self.total, {"productDisplayName": recommended_names})
            recommended_items = [row["id"] for row in recommended_data]
            self.result_label.config(text=f"Recommended items: {', '.join(recommended_names)}")

            # Disable choices for recommended items
            for category in self.category_order:
                valid_choices = sorted(list(self.filter_choices(category)))
                menu = self.choices[category]['menu']
                for index, choice in enumerate(valid_choices):
                    if any(row[category] == choice for row in recommended_data):
                        menu.entryconfig(index, state="disabled")


if __name__ == "__main__":
    ClothingRecommenderGUI()