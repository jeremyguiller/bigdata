# BigData

# Liste des tâches à réaliser
### Collecte de données
- [ ] Approches automatisées de la collecte de données
- [ ] Utilisation d'images sous licence libre
- [ ] Stockage et gestion des images et des métadonnées associées
### Étiquetage et annotation
- [ ] Approches automatisées de l'étiquetage
- [ ] Stockage et gestion des étiquettes et des annotations des images
- [ ] Utilisation d'algorithmes de classification et de regroupement
### Analyses de données
- [ ] Types d'analyses utilisées
- [ ] Utilisation de Pandas et Scikit-learn
- [ ] Utilisation d'algorithmes d'exploration de données
### Visualisation des données
- [ ] Types de techniques de visualisation utilisées
- [ ] Utilisation de matplotlib
### Système de recommandation
- [ ] Stockage et gestion des préférences et du profil de l'utilisateur
- [ ] Utilisation d'algorithmes de recommandation
### Tests
- [ ] Présence de tests fonctionnels
- [ ] Présence de tests utilisateurs
### Rapport
- [ ] Clarté de la présentation
- [ ] Présence d'une introduction et d'une conclusion claires, architecture des diagrammes, un résumé des différentes tâches réalisées et des limites
- [ ] Bibliographie